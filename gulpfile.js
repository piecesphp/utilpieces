/*
* Dependencias
*/
var gulp = require('gulp')
var typescript = require('gulp-typescript');
var watch = require('gulp-watch');
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')

gulp.task('compile', function () {
	compile()
})

gulp.task('watch', function () {
	watch(['src/*.ts']).pipe(() => {
		compile()
	})
})

function compile() {
	console.time('Duración de la compilación')
	let files = gulp.src(['src/*.ts'])
	let soucermapInit = files.pipe(sourcemaps.init())
	let compiled = soucermapInit.pipe(typescript({
		target: 'es5',
	}))
	//.pipe(uglify())
	let concated = compiled.pipe(concat('util-pieces.js'))

	concated.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('dist/'))
		.on('end', () => {
			console.timeEnd('Duración de la compilación')
		})
}